import java.util.Scanner;

public class Principal {
    public static void main(String[] args) {
        Persona p;
        p = new Persona(123, "Pepe");
        System.out.println(p.getDni());
        System.out.println(p.nombre);
        System.out.println(p);

    }
}


class Persona {
    private int dni;
    public String nombre;


    public Persona(int dni, String nombre) {
        this.dni = dni;
        this.nombre = nombre;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "dni=" + dni +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}