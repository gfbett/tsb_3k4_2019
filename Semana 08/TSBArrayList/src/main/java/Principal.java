import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class Principal {

    public static void main(String[] args) {
        List<Integer> l = new TSBArrayList<>();

        for (int i = 0; i < 20; i++) {
            l.add(i);
        }

        l.forEach(o -> System.out.println(o * 2));
        List<Persona> p = l.stream()
                .map(Persona::new)
                .collect(Collectors.toList());
        System.out.println(p);
    }
}
