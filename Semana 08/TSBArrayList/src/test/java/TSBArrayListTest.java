import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TSBArrayListTest {

    private TSBArrayList<Integer> list;

    @Before
    public void setup() {
        Integer[] v = {1, 2, 3, 4};
        list = new TSBArrayList<>(v);
    }

    @Test()
    public void testListInmutable() {
        boolean res = list.add(3);
        assertTrue(res);
        assertEquals(5, list.size());
        assertEquals(Integer.valueOf(3), list.get(4));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testListRemove() {
        list.remove(0);
    }

    @Test
    public void testSize() {
        assertEquals(4, list.size());
    }

    @Test
    public void addSeveralElements() {
        for (int i = 0; i < 100; i++) {
            list.add(i);
        }
        assertEquals(104, list.size());
    }

    @Test
    public void testGet() {
        assertEquals(Integer.valueOf(1), list.get(0));
        assertEquals(Integer.valueOf(2), list.get(1));
        assertEquals(Integer.valueOf(3), list.get(2));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetNegativo() {
        list.get(-1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetMayor() {
        list.get(4);
    }


}