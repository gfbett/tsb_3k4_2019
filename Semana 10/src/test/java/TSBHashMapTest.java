import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class TSBHashMapTest {

    private Map<Integer, String> map;

    @Before
    public void setup() {
        map = new TSBHashMap<Integer, String>();
    }

    @Test
    public void testSizeAndPut() {
        assertEquals(0, map.size());
        map.put(123, "Pepe");
        assertEquals(1, map.size());
    }

    @Test
    public void testIsEmpty() {
        assertTrue(map.isEmpty());
        map.put(123, "Pepe");
        assertFalse(map.isEmpty());
    }

    @Test
    public void testGet() {
        String value = "Pepe";
        map.put(123, value);
        assertEquals(value, map.get(123));
    }

}