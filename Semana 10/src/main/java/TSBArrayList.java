import java.util.AbstractList;
import java.util.RandomAccess;

public class TSBArrayList<E> extends AbstractList<E> implements RandomAccess {

    private E[] v;
    private int size;

    public TSBArrayList() {
        v = (E[]) new Object[10];
    }

    public TSBArrayList(E[] other) {
        v = (E[]) new Object[other.length * 2];
        System.arraycopy(other, 0, v, 0, other.length);
        size = other.length;
    }

    @Override
    public boolean add(E e) {
        ensureCapacity(size + 1);
        v[size] = e;
        size ++;
        return true;
    }

    private void ensureCapacity(int expected) {
        if (v.length <= expected) {
            E[] aux = (E[]) new Object[expected * 2];
            System.arraycopy(v, 0 , aux, 0, size);
            v = aux;
        }
    }

    public E get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return v[index];
    }

    public int size() {
        return size;
    }
}
