import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class TSBHashMap<K, V> implements Map<K, V> {

    private TSBArrayList<Map.Entry<K, V>>[] table;
    private int size;

    public TSBHashMap() {
        table = new TSBArrayList[10];
        for (int i = 0; i < table.length; i++) {
            table[i] = new TSBArrayList<>();
        }
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean containsKey(Object key) {
        return false;
    }

    public boolean containsValue(Object value) {
        return false;
    }

    public V get(Object key) {
        int pos = h(key);
        for(Map.Entry<K, V> x : table[pos]) {
            if(x.getKey().equals(key)) {
                return x.getValue();
            }
        }
        return null;
    }

    public V put(K key, V value) {
        int pos = h(key);
        for(Map.Entry<K, V> x : table[pos]) {
            if(x.getKey().equals(key)) {
                return x.setValue(value);
            }
        }
        table[pos].add(new TSBEntry(key, value));
        size ++;
        return null;
    }

    public V remove(Object key) {
        return null;
    }

    public void putAll(Map<? extends K, ? extends V> m) {

    }

    public void clear() {

    }

    public Set<K> keySet() {
        return null;
    }

    public Collection<V> values() {
        return null;
    }

    public Set<Entry<K, V>> entrySet() {
        return null;
    }

    private int h(Object key) {
        return Math.abs(key.hashCode()) % table.length;
    }

    private class TSBEntry implements Map.Entry<K, V> {

        private K key;
        private V value;

        public TSBEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V old = this.value;
            this.value = value;
            return old;
        }
    }

}
