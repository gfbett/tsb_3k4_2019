public class Persona {


    final int dni;

    public Persona(int dni) {
        this.dni = dni;
    }

    public  Persona() {
        dni = 0;
    }

    public final void mostrar() {

    }

    @Override
    public String toString() {
        return "Persona{" +
                "dni=" + dni +
                '}';
    }
}
