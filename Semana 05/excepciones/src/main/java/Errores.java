import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Errores {

    public static void main(String[] args) {
        System.out.println("antes main");
        test1();
        System.out.println("despues main");
    }

    private static void test1() {
        System.out.println("antes t1");

        test2();
        System.out.println("despues t1");

    }

    private static void test2() {
        System.out.println("antes t2");

        test3();
        System.out.println("despues t2");

    }

    private static void test3() {
        System.out.println("antes t3");
        try {
            test4();
        } catch (NumberFormatException e) {
            System.out.println("Error, valor inválido");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("despues t3");

    }

    private static void test4()  {
        System.out.println("antes t4");

        test5();

        System.out.println("despues t4");

    }

    private static void test5()  {
        System.out.println("antes t5");

        try (Scanner sc = new Scanner(new File("datos.txt")))
        {
            sc.nextInt();
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Errorr!!", e);
        }

        System.out.println("despues t5");
    }
}
