public class Lista {

    private Nodo frente;

    public void addFirst(Object x) {
        isHomogeneus(x);

        Nodo n = new Nodo(x);
        n.next = frente;
        frente = n;
    }

    private void isHomogeneus(Object x) {
        if (frente != null && frente.info.getClass() != x.getClass()) {
            throw new RuntimeException("Error!");
        }
    }

    public Object getFirst() {
        if (frente != null) {
            return frente.info;
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        Nodo p = frente;
        while(p != null) {
            res.append(p.info).append(", ");
            p = p.next;
        }
        return res.toString();
    }
}
