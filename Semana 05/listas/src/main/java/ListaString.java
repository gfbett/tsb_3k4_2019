public class ListaString {

    Lista l = new Lista();

    public void addFirst(String x) {
        l.addFirst(x);
    }

    public String getFirst() {
        return (String) l.getFirst();
    }

    @Override
    public String toString() {
        return l.toString();
    }
}
