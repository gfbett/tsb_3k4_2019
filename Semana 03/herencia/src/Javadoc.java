/**
 * clase de ejemplo de javadoc
 * @since 1.0
 * @author fede
 */
public class Javadoc {

    public static void main(String[] args) {
        test();
        System.out.println(sumar(10,23));
    }

    /**
     * Este método no hace nada
     */
    private static void test() {


    }

    /**
     * Este método suma dos números
     * @param a Un número a sumar
     * @param b Otro número a sumar
     * @return La suma de a+b
     */
    private static int sumar(int a, int b) {
        return a + b;
    }

}


