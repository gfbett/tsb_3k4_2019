public class Casting {
    public static void main(String[] args) {

        long a = 88223434565l;
        float x = 3.14f;

        int b = (int) a;
        System.out.println(b);

        char z = 'ك';
        int c = z;
        System.out.println(c);

    }

}
