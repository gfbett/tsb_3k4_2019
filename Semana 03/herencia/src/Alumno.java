public class Alumno extends Persona {

    private int legajo;

    public Alumno() {
        this(1234, 123, "Pepe");
    }

    public Alumno(int legajo, int dni, String nombre) {
        super(dni, nombre);
        this.legajo = legajo;
    }


    public int getLegajo() {
        return legajo;
    }

    public void setLegajo(int legajo) {
        this.legajo = legajo;
    }

    @Override
    public String toString() {
        return "Alumno{" +
                "dni=" + getDni() +
                ", nombre='" + getNombre() + '\'' +
                ", legajo=" + legajo +
                '}';
    }
}
