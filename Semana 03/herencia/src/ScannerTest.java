import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ScannerTest {

    public static void main(String[] args) {

        File file = new File("datos.txt");
        try (Scanner sc = new Scanner(file)) {
            int suma = 0;
            while (sc.hasNextInt()) {
                int nro = sc.nextInt();
                suma += nro;
            }
            System.out.println("La suma es: " + suma);

        } catch (FileNotFoundException e) {
            System.err.println("ERRORR!!!!!!" + e.getMessage());
        }


    }
}
