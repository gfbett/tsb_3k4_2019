import java.util.Arrays;
import java.util.Iterator;

public class TSBArrayList<E> {

    private E[] v;
    private int size;

    public TSBArrayList() {
        v = (E[]) new Object[10];
        size = 0;
    }

    public int size() {
        return size;
    }

    boolean isEmpty() {
        return size == 0;
    }

    void clear() {
        for (int i = 0; i < size; i++) {
            v[i] = null;
        }
        size = 0;
    }

    public void add(E x) {
        ensureCapacity();
        v[size] = x;
        size++;
    }

    private void ensureCapacity() {
        if (size == v.length) {
            E[] aux = (E[]) new Object[v.length * 2];
            System.arraycopy(v, 0, aux, 0, size);
            v = aux;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[");

        for (int i = 0; i < size; i++) {
            sb.append(v[i]);
            sb.append(", ");
        }

        sb.append("]");
        return sb.toString();
    }

    public E get(int index) {
        if (index < 0 || index >= size) {
            throw new IllegalArgumentException("Indice fuera de rango");
        }
        return v[index];
    }

    void add(int index, E x) {
        if (index < 0 || index > size) {
            throw new IllegalArgumentException("Indice fuera de rango");
        }
        ensureCapacity();
        System.arraycopy(v, index, v, index + 1, size - index);
        v[index] = x;
        size++;
    }

    public E set(int index, E x) {
        if (index < 0 || index >= size) {
            throw new IllegalArgumentException("Indice fuera de rango");
        }
        E old = v[index];
        v[index] = x;
        return old;
    }

    boolean contains(Object x) {
        boolean res = false;
        for(int i = 0; i < size; i++) {
            if (v[i].equals(x)) {
                res = true;
                break;
            }
        }
        return res;
    }

    E remove(int index) {
        if (index < 0 || index >= size) {
            throw new IllegalArgumentException("Indice fuera de rango");
        }
        E old = v[index];
        System.arraycopy(v, index+1, v, index, size - index+1);
        size--;
        return old;
    }

    Iterator iterator() {
        return new TSBArrayListIterator();
    }

    private class TSBArrayListIterator implements Iterator {

        private int current;
        private boolean canRemove;

        @Override
        public boolean hasNext() {
            canRemove = false;
            return current < size;
        }

        @Override
        public Object next() {
            E value = v[current];
            current++;
            canRemove = true;
            return value;
        }

        @Override
        public void remove() {
            if (!canRemove) {
                throw new IllegalStateException();
            }
            TSBArrayList.this.remove(current-1);
            canRemove = false;
            current--;
        }
    }
}
