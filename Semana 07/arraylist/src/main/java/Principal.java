import java.util.Iterator;

public class Principal {

    public static void main(String[] args) {

        TSBArrayList<Integer> l = new TSBArrayList<>();

        assertEquals(0, l.size(), "Size debe ser cero");

        l.add(1);

        assertEquals(1, l.size(), "Size debe ser uno");
        for (int i = 0; i < 10; i++) {
            System.out.println(l);
            l.add(i);
        }
        assertEquals(1, l.get(0), "El elemento debe ser 0");
        System.out.println(l);

        for (int i = 0; i < 10; i++) {
            l.add(1, i);
            System.out.println(l);
        }

        int old = l.set(1, 42);
        assertEquals(9, old, "debe ser nueve");

        System.out.println(l);

        System.out.println("Está el 42:" + l.contains(42));
        System.out.println("Está el 43:" + l.contains(43));
        old = l.set(1, 42234);
        System.out.println("Está el 42234:" + l.contains(42234));
        System.out.println(l);
        old = l.remove(3);
        assertEquals(7, old, "debería ser 7");
        System.out.println(l);
        Iterator iterator = l.iterator();
        while(iterator.hasNext()) {
            Integer value = (Integer) iterator.next();
            System.out.println(value);
            if (value.equals(3)) {
                iterator.remove();
            }
        }
        System.out.println(l);

        l.clear();

        assertEquals(0, l.size(), "Size debe ser cero");
        System.out.println("Todo OK!");
    }


    private static void assertEquals(int a, int b, String mensaje) {
        if (a != b)
            throw new RuntimeException(mensaje);
    }
}
