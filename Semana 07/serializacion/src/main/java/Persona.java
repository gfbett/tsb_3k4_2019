import java.io.Serializable;
import java.util.Arrays;

public class Persona implements Serializable {

    private static final long serialVersionUID = 2L;

    private int dni;
    private String nombre;
    private transient Persona[] parientes = new Persona[10];
    private int cantidadParientes = 0;

    public Persona(int dni, String nombre) {
        this.dni = dni;
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Persona\n {" +
                "dni=" + dni +
                "\n, nombre='" + nombre + '\'' +
                "\n, parientes=" + Arrays.toString(parientes) +
                "\n, cantidadParientes=" + cantidadParientes +
                '}';
    }

    public void addPariente(Persona p) {
        parientes[cantidadParientes++] = p;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void test() {

    }
}
