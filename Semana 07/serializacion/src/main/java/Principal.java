import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Principal {

    public static void main(String[] args) {

        Persona p = new Persona(123, "Pepe");
        Persona q = new Persona(234, "Laura");
        p.addPariente(q);
        q.addPariente(p);

        try {
            File file = new File("datos.bin");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(p);

        } catch (IOException ex) {
            System.err.println("Error grabando: " + ex.getMessage());
            ex.printStackTrace();
        }

    }
}
