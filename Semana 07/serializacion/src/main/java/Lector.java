import java.io.*;

public class Lector {

    public static void main(String[] args) {

        try {
            File file = new File("datos.bin");
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream oos = new ObjectInputStream(fis);

            Persona p = (Persona) oos.readObject();
            System.out.println(p);

        } catch (IOException | ClassNotFoundException ex) {
            System.err.println("Error grabando: " + ex.getMessage());
            ex.printStackTrace();
        }

    }
}
