import java.util.Scanner;

public class Menu {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int op;
        do {
            mostrarMenu();
            op = sc.nextInt();
            switch (op) {
                case 1:
                    System.out.println("Opcion 1");
                    break;
                case 2:
                    System.out.println("Opcion 2");
                    break;
                case 3:
                    System.out.println("Opcion 3");
                    break;
                case 4:
                    System.out.println("Opcion 4");
                    break;
            }
        }
        while(op != 4);

    }

    private static void mostrarMenu() {
        System.out.println("1 _ Opcion 1");
        System.out.println("2 _ Opcion 2");
        System.out.println("3 _ Opcion 3");
        System.out.println("4 _ Salir");

    }

}
