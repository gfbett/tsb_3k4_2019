public class Switch {
    public static void main(String[] args) {
        char z = 'z';

        switch (z) {
            case 'a':
            case 'A':
                System.out.println("a");
                break;
            case 2:
                System.out.println("2");
                break;
            case 3:
                System.out.println("3");
            case 4:
                System.out.println("4");
                break;
            default:
                System.out.println("Ninguno");
        }

        int a = 3;
        int b = ++a;
        System.out.println("A:" + a + " B:" + b);
    }
}
