import java.util.Iterator;

public class Lista<T> implements Iterable<T> {

    private Nodo frente;
    private int length;

    public Lista() {

    }

    public int size() {
        return length;
    }

    public void addFirst(T x) {
        Nodo<T> n = new Nodo<>(x);
        n.next = frente;
        frente = n;
        length++;
    }

    public T get(int ix) {
        if(ix < 0 || ix >= length)
            throw new IllegalArgumentException("Indice inválido");
        Nodo<T> p = frente;
        for(int i=0; i<ix; i++) {
            p = p.next;
        }
        return p.info;

    }



    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Nodo p = frente;
        while (p != null) {
            sb.append(p.info);
            sb.append(", ");
            p = p.next;
        }
        return sb.toString();
    }

    @Override
    public Iterator<T> iterator() {
        return new IteradorLista<>();
    }


    private static class Nodo<T> {
        T info;
        Nodo next;

        public Nodo(T info) {
            this.info = info;
        }
    }

    private class IteradorLista<T> implements Iterator<T> {

        private Lista.Nodo<T> actual;

        public IteradorLista() {
            this.actual = frente;
        }

        public boolean hasNext() {
            return actual != null;
        }

        public T next() {
            T info = actual.info;
            actual = actual.next;
            return info;
        }
    }

}

