import javax.swing.*;

public class Principal {

    public static void main(String[] args) {

        Lista<Integer> l = new Lista<>();
        for (int i = 0; i < 100000; i++) {
            l.addFirst(i);
        }
        System.out.println(l);
//        System.out.println("Suma con get:");
//        System.out.println("La suma es: " + suma_get(l));
        System.out.println("Suma con Iterator:");
        System.out.println("La suma es: " + suma_iterator(l));
        JOptionPane.showMessageDialog(null,  "Hola");
    }

    public static int suma_get(Lista l) {
        int suma = 0;
        for(int i = 0; i < l.size(); i++) {
            suma += (int)l.get(i);
        }
        return suma;
    }

    public static int suma_iterator(Lista l) {
        int suma = 0;
        for (Object i: l) {
            suma += (int)i;
        }
        return suma;
    }
}
