public class Multiplicacion extends Operador {
    public Multiplicacion(Valuable derecha, Valuable izquierda) {
        super(derecha, izquierda);
    }

    public int valuar() {
        return izquierda.valuar() * derecha.valuar();
    }
}
