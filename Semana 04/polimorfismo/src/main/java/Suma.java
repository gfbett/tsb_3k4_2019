public class Suma extends Operador {
    public Suma(Valuable derecha, Valuable izquierda) {
        super(derecha, izquierda);
    }

    public int valuar() {
        return izquierda.valuar() + derecha.valuar();
    }
}
