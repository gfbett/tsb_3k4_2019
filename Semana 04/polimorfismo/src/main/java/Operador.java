public abstract class Operador implements Valuable {

    protected Valuable derecha;
    protected Valuable izquierda;

    public Operador(Valuable derecha, Valuable izquierda) {
        this.derecha = derecha;
        this.izquierda = izquierda;
    }
}
