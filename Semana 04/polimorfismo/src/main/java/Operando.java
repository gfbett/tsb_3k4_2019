public class Operando implements Valuable {

    private int valor;

    public Operando(int valor) {
        this.valor = valor;
    }

    @Override
    public int valuar() {
        return valor;
    }
}
