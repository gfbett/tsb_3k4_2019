import org.junit.*;


public class OperacionesTest {

    @Test
    public void testSumar() {

        int res = Operaciones.sumar(1, 2);
        Assert.assertEquals(3, res);
    }

}